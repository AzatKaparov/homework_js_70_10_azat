import './App.css';
import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MainContainer from "./Containers/MainContainer";
import Layout from "./Components/UI/Layout/Layout";

function App() {
  return (
      <div className="App">
        <Layout>
          <BrowserRouter>
            <Switch>
              <Route path="/" exact component={MainContainer}/>
              <Route render={() => <h1>Not found</h1>}/>
            </Switch>
          </BrowserRouter>
        </Layout>
      </div>
  );
}

export default App;