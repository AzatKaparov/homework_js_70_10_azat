import React from 'react';
import './CustomerForm.css';

const CustomerForm = ({customer, dataChange, sendOrder}) => {
    return (
        <form onSubmit={sendOrder} className="CustomerForm">
            <p>Enter your contacts</p>
            <input
                onChange={dataChange}
                value={customer.name}
                placeholder="Name"
                type="text"
                name="name"
            />
            <input
                onChange={dataChange}
                value={customer.phone}
                placeholder="Phone"
                type="phone"
                name="phone"
            />
            <input
                onChange={dataChange}
                value={customer.address}
                placeholder="Address"
                type="text"
                name="address"
            />
            <button onClick={sendOrder}>Submit</button>
        </form>
    );
};

export default CustomerForm;