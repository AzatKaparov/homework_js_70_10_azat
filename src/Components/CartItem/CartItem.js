import React from 'react';
import './CartItem.css';

const CartItem = ({name, amount, price, deleted}) => {
    return (
        <div onClick={deleted} className="CartItem d-flex justify-content-between">
            <p>{name}</p>
            <p>x{amount}</p>
            <p>{price * amount}</p>
        </div>
    );
};

export default CartItem;