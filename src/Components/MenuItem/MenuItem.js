import React from 'react';
import './MenuItem.css';

const MenuItem = ({name, price, img, clicked}) => {
    return (
        <div className="MenuItem">
            <img src={img} alt={name}/>
            <div className="text-block">
                <p>
                    {name}
                    <span>{price} KGS</span>
                </p>
            </div>
            <button onClick={clicked}>Add to cart</button>
        </div>
    );
};

export default MenuItem;