import React from 'react';
import './Cart.css';
import {useDispatch, useSelector} from "react-redux";
import CartItem from "../CartItem/CartItem";
import {removeFromCart} from "../../store/actions/cartActions";

const Cart = ({showModal}) => {
    const {cartProducts} = useSelector(state => state.cart);
    const {products} = useSelector(state => state.products);
    const dispatch = useDispatch();
    let cart;
    let totalPrice = 0;

    const deleteProduct = name => {
        dispatch(removeFromCart(name));
    };

    if (cartProducts.length === 0) {
        cart = <h5>There is no any products in your cart</h5>;
    } else {
        cart = cartProducts.map((item, idx) => {
            return products.map(prod => {
                if (item.name === prod.name) {
                    return (
                        <CartItem
                        name={item.name}
                        amount={item.amount}
                        price={prod.price}
                        deleted={() => deleteProduct(item.name)}
                        key={idx}/>);
                }
                return null;
            });
        });
        products.forEach(product => {
            cartProducts.forEach(cartProduct => {
                if (product.name === cartProduct.name) {
                    totalPrice += (cartProduct.amount * product.price);
                }
            })
        })
        totalPrice += 150;
    }

    return (
        <div className="Cart d-flex flex-column align-items-center justify-content-start w-25">
            <h4>Cart</h4>
            {cartProducts.length > 0 &&
            <div className="price-block">
                <h4>Delivery: 150</h4>
                <h4>Total: {totalPrice}</h4>
            </div>
            }
            {cart}
            {cartProducts.length > 0 &&
                <button onClick={showModal} className="btn-order">Place order</button>
            }
        </div>
    );
};

export default Cart;