import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://homework-70-default-rtdb.firebaseio.com/'
});

export default axiosApi;