import {FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS} from "./actions/productsActions";

const initialState = {
    products: [],
    loading: null,
    error: null,
};

const parseProducts = obj => {
    return Object.keys(obj).map(key => {
        return {...obj[key], id: key}
    });
};

const productsReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_REQUEST:
            return {...state, loading: true};
        case FETCH_PRODUCTS_SUCCESS:
            const parsedData = parseProducts(action.products);
            return {...state, products: parsedData, loading: false};
        case FETCH_PRODUCTS_ERROR:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default productsReducer;