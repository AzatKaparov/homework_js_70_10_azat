import {ADD_TO_CART, INIT_CART, REMOVE_FROM_CART} from "./actions/cartActions";

const initialState = {
    cartProducts: [],
};

const cartReducer = (state=initialState, action) => {
    switch (action.type) {
        case INIT_CART:
            return {...state, cartProducts: initialState.cartProducts};
        case ADD_TO_CART:
            let changed = false;
            const existingState = state.cartProducts.map(item => {
                if (item.name === action.product.name) {
                    changed = true;
                    return {
                        ...item,
                        amount: item.amount + 1
                    };
                }
                return item;
            })
            if (changed) {
                return {...state, cartProducts: existingState};
            } else {
                const newProduct = {
                    name: action.product.name,
                    amount: 1,
                };
                return {...state, cartProducts: [...state.cartProducts, newProduct]};
            }
        case REMOVE_FROM_CART:
            return {...state, cartProducts: state.cartProducts.filter(item => item.name !== action.name)};
        default:
            return state;
    }
};

export default cartReducer;