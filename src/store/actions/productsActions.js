import axiosApi from "../../axiosApi";

export const FETCH_PRODUCTS_REQUEST = 'FETCH_PRODUCTS_REQUEST';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_ERROR = 'FETCH_PRODUCTS_ERROR';

export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products: products});
export const fetchProductsError = error => ({type: FETCH_PRODUCTS_ERROR, error: error});


export const fetchProducts = () => {
    return async dispatch => {
        dispatch(fetchProductsRequest());

        try {
            const response = await axiosApi.get('./products.json');
            dispatch(fetchProductsSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductsError(e))
        }
    }
}