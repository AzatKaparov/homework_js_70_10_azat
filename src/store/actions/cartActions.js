export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const INIT_CART = "INIT_CART";

export const addToCart = product => ({type: ADD_TO_CART, product: product});
export const removeFromCart = name => ({type: REMOVE_FROM_CART, name: name});
export const initCart = () => ({type: INIT_CART});