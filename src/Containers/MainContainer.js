import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../store/actions/productsActions";
import MenuItem from "../Components/MenuItem/MenuItem";
import Preloader from "../Components/UI/Preloader/Preloader";
import Backdrop from "../Components/UI/Backdrop/Backdrop";
import Cart from "../Components/Cart/Cart";
import {addToCart, initCart} from "../store/actions/cartActions";
import Modal from "../Components/UI/Modal/Modal";
import CustomerForm from "../Components/CustomerForm/CustomerForm";
import axiosApi from "../axiosApi";

const MainContainer = () => {
    const dispatch = useDispatch();
    const {products, loading} = useSelector(state => state.products);
    const {cartProducts} = useSelector(state => state.cart);
    const [showModal, setShowModal] = useState(false);

    const [customer, setCustomer] = useState({
        name: "",
        phone: "",
        address: "",
    });

    const sendOrder = async e => {
        e.preventDefault();
        const order = {
            customer: {...customer},
            products: {...cartProducts}
        };
        try {
            await axiosApi.post('./orders.json', order);
        } finally {
            dispatch(initCart())
            setShowModal(false);
        }
    };

    const dataChanged = e => {
        const name = e.target.name;
        const value = e.target.value;

        setCustomer(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const addProduct = product => {
        dispatch(addToCart(product));
    };

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch])

    return (
        <>
            <Modal show={showModal} close={() => setShowModal(!showModal)}>
                <CustomerForm
                    customer={customer}
                    dataChange={dataChanged}
                    sendOrder={sendOrder}
                />
            </Modal>
            <Preloader show={loading} />
            <Backdrop show={loading}/>
            <div style={{maxHeight: 1200}} className="container py-5 justify-content-between d-flex bg-white">
                <div style={{overflowY: 'scroll'}} className="menu w-75 d-flex flex-wrap">
                    {products.map(item => (
                        <MenuItem
                            key={item.id}
                            name={item.name}
                            price={item.price}
                            img={item.img}
                            clicked={() => addProduct(item)}
                        />
                    ))}
                </div>
                <Cart showModal={() => setShowModal(!showModal)}/>
            </div>
        </>
    );
};

export default MainContainer;